crypt4j
=======

Description
-----------

A Java library implementing unix password hash generation.
Supports DES and MD5.


License
-------

This library is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the
[GNU Lesser General Public License][2] for more details.


Download
--------

The JAR can be downloaded from my [Maven Repository][1] or if you are
using Maven you can simply add it as a dependency:

    <repositories>
      <repository>
        <id>ailis-releases</id>
        <name>Ailis Maven Releases</name>
        <url>http://nexus.ailis.de/content/groups/public/</url>
      </repository>
    </repositories>

    <dependencies>
      <dependency>   
        <groupId>de.ailis.crypt4j</groupId>
        <artifactId>crypt4j</artifactId>
        <version>1.0.1</version>
      </dependency>
    </dependencies>


Usage
-----

    // Generates a MD5 password hash with a random salt: 
    cryptedPassword = Crypt.crypt(clearTextPassword); 
    // or:
    cryptedPassword = MD5Crypt.crypt(clearTextPassword); 
    
    // Generates a DES password hash with a random salt: 
    cryptedPassword = Crypt.crypt(clearTextPassword); 
    // or:
    cryptedPassword = DESCrypt.crypt(clearTextPassword); 
    
    // Generates a MD5 password hash with the specified salt:
    cryptedPassword = Crypt.crypt(clearTextPassword, "$1$abcdefgh");
    // or:
    cryptedPassword = MD5Crypt.crypt(clearTextPassword, "abcdefgh");

    // Generates a DES password hash with the specified salt:
    cryptedPassword = Crypt.crypt(clearTextPassword, "abcdefgh");
    // or:
    cryptedPassword = DESCrypt.crypt(clearTextPassword, "abcdefgh");

[1]: http://nexus.ailis.de/content/repositories/releases/de/ailis/crypt4j/crypt4j/ "Maven Repository"
[2]: http://www.gnu.org/licenses/lgpl.html "License"
